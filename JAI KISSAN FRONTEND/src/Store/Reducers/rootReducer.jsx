// npm dependencies
import { combineReducers }          from 'redux'

//reducer imports

/**
 * Combine all reducers into root reducer
 */
const rootReducer = combineReducers({
  
});

export default rootReducer;