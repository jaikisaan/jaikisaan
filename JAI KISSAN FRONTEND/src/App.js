// npm dependencies
import React, { Component }      from "react";

// routes for the main App module
import Routes                    from "./routes"; 

/**
 * This class carries routes component.
 */
class App extends Component {
  render() {
    return (     
      <Routes />
    );
  }
}

export default App;