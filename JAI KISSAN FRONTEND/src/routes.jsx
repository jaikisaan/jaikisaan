// npm dependencies
import React, { Component }             from "react";
import { BrowserRouter }                from "react-router-dom";

// all components for routing
//component imports goes here.....


/**
 * This is the routes component 
 */
class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>

          {/* defining route paths and assigning components to respective paths  */}

        </React.Fragment>
      </BrowserRouter>
    );
  }
}
export default Routes;