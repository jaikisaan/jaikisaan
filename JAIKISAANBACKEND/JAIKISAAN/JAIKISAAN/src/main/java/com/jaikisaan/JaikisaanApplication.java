package com.jaikisaan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JaikisaanApplication {

	public static void main(String[] args) {
		SpringApplication.run(JaikisaanApplication.class, args);
	}

}
